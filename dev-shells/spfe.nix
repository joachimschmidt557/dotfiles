{ pkgs }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    cmake
    gmp
    boost166
    openssl
    doxygen
    ninja
    llvmPackages_13.clang
    perl534Packages.LWP
    bison
  ];
}
