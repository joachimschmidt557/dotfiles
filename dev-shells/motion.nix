{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    cmake
    gmp
    boost173
    openssl
    doxygen
    graphviz
    ninja
    clang_10
    flatbuffers
    fmt
    gtest
  ];
}
