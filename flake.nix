{
  description = "dotfiles";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.nixpkgs-bitwig = {
    url = "github:NixOS/nixpkgs/e0c0c1f08328a932fcc69bdf14c509646fd912c4";
    flake = false;
  };
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, nixpkgs-bitwig, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          system = system;
          config = { allowUnfree = true; };
        };
        pkgs-bitwig = import nixpkgs-bitwig {
          system = system;
          config = { allowUnfree = true; };
        };
      in {
        packages.mopidyWithExtensions = pkgs.buildEnv {
          name = "mopidy-with-extensions-${pkgs.mopidy.version}";
          paths = pkgs.lib.closePropagation
            (with pkgs; [ mopidy-iris mopidy-local mopidy-mpd ]);
          pathsToLink = [ "/${pkgs.mopidyPackages.python.sitePackages}" ];
          buildInputs = [ pkgs.makeWrapper ];
          postBuild = ''
            makeWrapper ${pkgs.mopidy}/bin/mopidy $out/bin/mopidy \
            --prefix PYTHONPATH : $out/${pkgs.mopidyPackages.python.sitePackages}
          '';
        };

        packages.conda-custom =
          pkgs.conda.override { extraPkgs = with pkgs; [ glib which ]; };

        packages.bitwig-studio3 =
          pkgs-bitwig.callPackage ./nix/bitwig-studio3.nix {
            bitwig-studio1 = pkgs-bitwig.callPackage ./nix/bitwig-studio1.nix {
              zenity = pkgs-bitwig.gnome3.zenity;
              libxkbcommon = pkgs-bitwig.libxkbcommon_7;
            };
          };

        devShells = {
          empty = import ./dev-shells/empty.nix { pkgs = pkgs; };
          empty-multi = import ./dev-shells/empty-multi.nix { pkgs = pkgs; };
          empty-multi-static =
            import ./dev-shells/empty-multi.nix { pkgs = pkgs.pkgsStatic; };
          empty-clang = import ./dev-shells/empty-clang.nix { pkgs = pkgs; };
          empty-clang-multi =
            import ./dev-shells/empty-clang-multi.nix { pkgs = pkgs; };

          basic-cmake = import ./dev-shells/basic-cmake.nix { pkgs = pkgs; };
          basic-rust = import ./dev-shells/basic-rust.nix { pkgs = pkgs; };

          aby = import ./dev-shells/aby.nix { pkgs = pkgs; };
          spfe = import ./dev-shells/spfe.nix { pkgs = pkgs; };
          zig-bootstrap =
            import ./dev-shells/zig-bootstrap.nix { pkgs = pkgs; };
          zig = import ./dev-shells/zig.nix { pkgs = pkgs; };
        };
      }) // {

        colmena = {

          meta = {
            nixpkgs = import nixpkgs {
              system = "x86_64-linux";
              overlays = [ ];
            };
          };

          dustbowl = { name, nodes, ... }: {
            networking.hostName = "dustbowl";

            deployment = {
              allowLocalDeployment = true;
              targetHost = null;
              privilegeEscalationCommand = [ "doas" ];
            };

            imports = [ (import ./nixos/dustbowl.nix { nixpkgs = nixpkgs; }) ];
          };

        };

        legacyPackages = nixpkgs.legacyPackages;

      };
}
