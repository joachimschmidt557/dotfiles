{ pkgs }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    ninja
    cmake
    python3
    zlib
  ];

  hardeningDisable = [ "all" ];
}
