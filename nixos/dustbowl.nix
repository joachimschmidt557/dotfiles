# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ nixpkgs }:
{ config, lib, pkgs, ... }:

{
  imports =
    [
      # Include the results of the hardware scan.
      /etc/nixos/hardware-configuration.nix
      /etc/nixos/private.nix
    ];

  nixpkgs.config.allowUnfree = true;
  nix = {
    package = pkgs.nixVersions.stable;
    extraOptions = ''
      experimental-features = nix-command flakes
      builders-use-substitutes = true
    '';
    distributedBuilds = true;
    registry.nixpkgs.flake = nixpkgs;
  };

  nixpkgs.overlays = [
    (final: prev: {
      soapyrtlsdr = prev.soapyrtlsdr.overrideAttrs (finalAttrs: prevAttrs: {
        src = prev.fetchFromGitHub {
          owner = "pothosware";
          repo = "SoapyRTLSDR";
          rev = "d5119cc3c9ddc10b5215710ff39bbae0ddf38c08";
          sha256 = "sha256-e0VF0jaDQ9ZHbOmsu93OX3qLB7pauWeMw6+/7olrX6o=";
        };
      });

      lutris = prev.lutris.override {
        extraPkgs = pkgs: [ pkgs.mangohud ];
      };
    })
  ];

  # Kernel version
  boot.kernelPackages = pkgs.linuxPackages_5_15;

  # Use the systemd-boot EFI boot loader.
  # boot.loader.systemd-boot.enable = true;
  boot.loader.efi = {
    canTouchEfiVariables = true;
    efiSysMountPoint = "/boot/efi";
  };
  boot.loader.grub = {
    enable = true;
    device = "nodev";
    efiSupport = true;
    enableCryptodisk = true;
  };
  boot.initrd.luks.devices = {
    root = {
      device = "/dev/disk/by-uuid/70c16b36-14b6-4939-9fc9-210774614e72";
      preLVM = true;
    };
  };
  boot.initrd.availableKernelModules = [
    "aesni_intel"
    "cryptd"
  ];

  # Enable systemd watchdog
  # systemd.watchdog = {
  #   runtimeTime = "30s";
  #   rebootTime = "10m";
  # };

  boot.extraModulePackages = [
    config.boot.kernelPackages.v4l2loopback.out
  ];

  boot.kernelModules = [
    "v4l2loopback"
  ];

  # For Focusrite Scarlett 2i2
  # See https://github.com/Focusrite-Scarlett-on-Linux/sound-usb-kernel-module
  boot.extraModprobeConfig = ''
    options snd_usb_audio vid=0x1235 pid=0x8210 device_setup=1
  '';

  # Add qemu-binfmt for ARM and AArch64
  boot.binfmt.emulatedSystems = [
    "armv7l-linux"
    "aarch64-linux"
    "riscv64-linux"
  ];

  services.udisks2.enable = true;

  services.fwupd.enable = true;

  networking.useNetworkd = true;
  networking.hostName = "dustbowl";
  networking.wireless.enable = true;
  networking.wireless.interfaces = [ "wlp2s0" ];

  networking.wireguard.enable = true;
  networking.firewall.checkReversePath = "loose";

  services.mullvad-vpn.enable = true;

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.wlp2s0.useDHCP = true;

  # Add TeleSec root certificate to /etc/ explicitly for eduroam
  environment.etc."ssl/certs/T-TeleSec_GlobalRoot_Class_2.pem".source = pkgs.fetchurl {
    url = "https://www.pki.dfn.de/fileadmin/PKI/zertifikate/T-TeleSec_GlobalRoot_Class_2.pem";
    hash = "sha256-swmJ/Z5Fx0v0F9900dpjnR8E1P0JAL6BOi1qAxpWyEU=";
  };

  # services.dnscrypt-proxy2 = {
  #   enable = true;
  #   settings = {
  #     require_nolog = true;
  #     require_nofilter = true;
  #     sources.public-resolvers = {
  #       urls = [ "https://download.dnscrypt.info/resolvers-list/v3/public-resolvers.md" ];
  #       cache_file = "public-resolvers.md";
  #       minisign_key = "RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3";
  #       refresh_delay = 72;
  #     };
  #   };
  # };

  # udev rules
  services.udev.packages = with pkgs; [
    yubikey-personalization
    openrgb
  ];

  # Configure console
  console = {
    font = "Lat2-Terminus16";
    keyMap = "adnw";
  };

  # Select internationalisation properties.
  i18n = {
    defaultLocale = "en_US.UTF-8";
  };

  # Set location (for wlsunset)
  location = {
    latitude = 49.5;
    longitude = 8.4;
  };

  environment.systemPackages = with pkgs; [
    ntfs3g

    # CLI utilities
    file
    usbutils
    pciutils
    calc
    wget
    unzip
    zip
    psmisc
    htop
    vim
    vis
    git
    git-lfs
    bubblewrap
    openconnect
    yubikey-personalization
    yubico-pam
    nmap
    bind.dnsutils
    gdb
    intel-gpu-tools
    # config.boot.kernelPackages.perf # FIXME broken on this nixpkgs version
    fd
    ripgrep
    jq
    fzf
    tig
    croc
    mullvad # just for the CLI, service is enabled elsewhere
    jless
    restic
    pdfgrep
    tmux
    sshuttle
    rclone
    timewarrior
    colmena

    # Services
    syncthing
    nextcloud-client
    mpd

    # GTK theme
    adwaita-icon-theme

    # Wayland utilities
    grim
    slurp
    wl-clipboard
    mako
    foot
    xdg-utils
    i3status
    wlr-randr
    waypipe
    wl-mirror

    # GUI software
    emacs29-pgtk
    firefox
    thunderbird
    mpv
    zathura
    imv
    ungoogled-chromium
    obs-studio
    virt-manager
    zotero
    wpa_supplicant_gui
    spotify
    sdrpp
    gqrx
    sdrangel
    lutris
    abracadabra
    pavucontrol

    # Messengers
    tdesktop
    signal-desktop
    element-desktop
    whatsapp-for-linux
    # fluffychat # FIXME insecure, see https://github.com/NixOS/nixpkgs/pull/334638#issuecomment-2289025802

    (callPackage ./posy-cursor-black.nix { })
  ];

  services.flatpak.enable = true;

  services.gnome.gnome-keyring.enable = true;

  # Use doas instead of sudo
  security.sudo.enable = false;
  security.doas = {
    enable = true;
    extraRules = [
      {
        groups = [ "wheel" ];
        persist = true;
        keepEnv = true;
      }
    ];
  };

  security.wrappers.rr = {
    owner = "root";
    group = "root";
    capabilities = "cap_perfmon=ip";
    source = "${pkgs.rr}/bin/rr";
  };

  security.wrappers.intel_gpu_top = {
    owner = "root";
    group = "root";
    capabilities = "cap_perfmon=p";
    source = "${pkgs.intel-gpu-tools}/bin/intel_gpu_top";
  };

  # TPM2 setup
  security.tpm2 = {
    enable = true;
    pkcs11.enable = true;
  };

  # Enable gnupg
  programs.gnupg.agent.enable = true;

  programs.fish.enable = true;

  programs.captive-browser = {
    enable = true;
    interface = "wlp2s0";
  };

  # Steam
  programs.steam.enable = true;

  # Open ports in the firewall.
  networking.firewall.enable = false;
  # networking.firewall.allowedTCPPorts = [ 8080 6600 ];
  # networking.firewall.allowedUDPPorts = [ 8080 ];

  # Enable CUPS to print documents.
  # services.printing.enable = true;
  # services.printing.drivers = [ pkgs.hplipWithPlugin ];

  hardware.sane = {
    enable = true;
    brscan4.enable = true;
    extraBackends = [ pkgs.hplipWithPlugin pkgs.sane-airscan ];
  };

  hardware.rtl-sdr.enable = true;

  systemd.user.services.wlsunset = {
    description = "wlsunset colour temperature adjuster";
    wantedBy = [ "graphical-session.target" ];
    partOf = [ "graphical-session.target" ];
    serviceConfig = {
      ExecStart = ''
        ${pkgs.wlsunset}/bin/wlsunset \
          -l ${toString config.location.latitude} \
          -L ${toString config.location.longitude} \
          -t 2000 \
      '';
      RestartSec = 3;
      Restart = "always";
    };
  };

  systemd.user.services.mpris-proxy = {
    description = "Bluetooth MPRIS Proxy";
    wantedBy = [ "graphical-session.target" ];
    partOf = [ "graphical-session.target" ];
    serviceConfig = {
      ExecStart = ''
        ${pkgs.bluez}/bin/mpris-proxy
      '';
      RestartSec = 3;
      Restart = "always";
    };
  };

  systemd.user.services.swayidle = {
    description = "swayidle idle manager for Wayland";
    wantedBy = [ "graphical-session.target" ];
    partOf = [ "graphical-session.target" ];
    path = with pkgs; [ bash sway swaylock ];
    serviceConfig = {
      ExecStart = ''
        ${pkgs.swayidle}/bin/swayidle -w \
          timeout 300 'swaylock -f -c 000000' \
          timeout 600 'swaymsg "output * dpms off"' \
          resume 'swaymsg "output * dpms on"' \
          before-sleep 'swaylock -f -c 000000'
      '';
      RestartSec = 3;
      Restart = "always";
    };
  };

  # Bluetooth
  hardware.bluetooth = {
    enable = true;
    settings = {
      General.Experimental = true;
    };
  };

  # UPower
  services.upower.enable = true;

  # RealtimeKit
  security.rtkit.enable = true;

  # Video acceleration
  hardware.graphics.extraPackages = with pkgs; [
    vaapiIntel
    intel-compute-runtime
  ];

  programs.wireshark.enable = true;

  # PipeWire
  services.pipewire = {
    enable = true;
    alsa = {
      enable = true;
      support32Bit = true;
    };
    pulse.enable = true;
  };

  # Sway
  programs.sway = {
    extraSessionCommands = ''
      export _JAVA_AWT_WM_NONREPARENTING=1
    '';
    wrapperFeatures.base = true;
    wrapperFeatures.gtk = true;
    enable = true;
  };

  services.logind.powerKey = "ignore";

  xdg.portal = {
    enable = true;
    extraPortals = with pkgs; [ xdg-desktop-portal-gtk ];
  };

  # Fonts
  fonts.packages = with pkgs; [
    fira-code
    noto-fonts
    noto-fonts-emoji
    noto-fonts-extra
    roboto
    roboto-slab
    roboto-mono
  ];
  fonts.fontconfig = {
    enable = true;
    defaultFonts = {
      emoji = [ "Noto Color Emoji" ];
      monospace = [ "Fira Code" ];
      sansSerif = [ "Noto Sans" ];
    };
  };

  virtualisation.libvirtd.enable = true;

  # virtualisation.virtualbox.host = {
  #   enable = true;
  #   enableExtensionPack = true;
  # };

  users.users.joachim = {
    isNormalUser = true;
    home = "/home/joachim";
    shell = pkgs.fish;
    extraGroups = [ "wheel" "scanner" "libvirtd" "wireshark" "vboxusers" "tss" "plugdev" "dialout" ];
  };

  # Yubikey PAM
  security.pam.yubico = {
    enable = true;
    mode = "challenge-response";
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "20.03"; # Did you read the comment?

}
