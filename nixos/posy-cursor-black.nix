{ lib, stdenv, fetchFromGitHub }:

stdenv.mkDerivation {
  pname = "posy-cursor-black";
  version = "unstable-2022-09-14";

  src = fetchFromGitHub {
    owner = "simtrami";
    repo = "posy-improved-cursor-linux";
    rev = "8c39b915bbba4717a450ef750fa8b0c0b60cb160";
    sha256 = "i0N/QB5uzqHapMCDl6h6PWPJ4GOAyB1ds9qlqmZacLY=";
  };

  patches = [ ./posy-cursors-rename.patch ];

  installPhase = ''
    mkdir -p $out/share/icons/posy-cursor-black
    cp Posy_Cursor_Black/index.theme $out/share/icons/posy-cursor-black/index.theme
    cp -r Posy_Cursor_Black/cursors $out/share/icons/posy-cursor-black/cursors
  '';
}
