DESTDIR=$(HOME)
PREFIX=/.config

fish: $(DESTDIR)$(PREFIX)/fish/config.fish

i3: $(DESTDIR)$(PREFIX)/i3/config

i3status: $(DESTDIR)$(PREFIX)/i3status/config

vis:
	install -Dm600 vis/visrc.lua $(DESTDIR)$(PREFIX)/vis/visrc.lua
	install -Dm700 vis/themes/* -t $(DESTDIR)$(PREFIX)/vis/themes/

Xresources:
	install -Dm600 Xresources $(DESTDIR)/.Xresources

fontconfig:
	install -Dm600 fontconfig/fonts.conf $(DESTDIR)$(PREFIX)/fontconfig/fonts.conf
	fc-cache

srcfile: $(DESTDIR)/src/.srcfile

gitconfig: $(DESTDIR)/.gitconfig

zls: $(DESTDIR)$(PREFIX)/zls.json

alacritty: $(DESTDIR)$(PREFIX)/alacritty.yml

sway: $(DESTDIR)$(PREFIX)/sway/config

emacs: $(DESTDIR)/.emacs

gtk: $(DESTDIR)$(PREFIX)/gtk-3.0/settings.ini

mpd: $(DESTDIR)$(PREFIX)/mpd.conf

foot: $(DESTDIR)$(PREFIX)/foot/foot.ini

nimmm: $(DESTDIR)$(PREFIX)/nimmm.conf

zshrc: $(DESTDIR)/.zshrc

.PHONY: fish i3 i3status vis Xresources fontconfig srcfile gitconfig zls alacritty sway emacs gtk mpd foot nimmm zshrc


## Actual file operations

$(DESTDIR)$(PREFIX)/fish/config.fish: $(PWD)/fish/nix.fish
	ln -sf $^ $@

$(DESTDIR)$(PREFIX)/i3/config: $(PWD)/i3/config
	ln -sf $^ $@

$(DESTDIR)$(PREFIX)/i3status/config: $(PWD)/i3status/config
	ln -sf $^ $@

$(DESTDIR)/src/.srcfile: $(PWD)/srcfiles/src
	ln -sf $^ $@

$(DESTDIR)/.gitconfig: $(PWD)/gitconfig
	ln -sf $^ $@

$(DESTDIR)$(PREFIX)/zls.json: $(PWD)/zls.json
	ln -sf $^ $@

$(DESTDIR)$(PREFIX)/alacritty.yml: $(PWD)/alacritty.yml
	ln -sf $^ $@

$(DESTDIR)$(PREFIX)/sway/config: $(PWD)/sway/config
	ln -sf $^ $@

$(DESTDIR)/.emacs: $(PWD)/emacs.el
	ln -sf $^ $@

$(DESTDIR)$(PREFIX)/gtk-3.0/settings.ini: $(PWD)/gtk-3.0/settings.ini
	ln -sf $^ $@

$(DESTDIR)$(PREFIX)/mpd.conf: $(PWD)/mpd.conf
	ln -sf $^ $@

$(DESTDIR)$(PREFIX)/foot/foot.ini: $(PWD)/foot.ini
	ln -sf $^ $@

$(DESTDIR)$(PREFIX)/nimmm.conf: $(PWD)/nimmm.conf
	ln -sf $^ $@

$(DESTDIR)/.zshrc: $(PWD)/zshrc
	ln -sf $^ $@
