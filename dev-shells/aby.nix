{ pkgs }:

pkgs.clangStdenv.mkDerivation {
  name = "";
  buildInputs = with pkgs; [
    cmake
    gmp
    boost166
    openssl
    doxygen
    ninja
  ];
}
