{ pkgs }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    cmake
    ninja
    llvmPackages_18.clang-unwrapped
    llvmPackages_18.llvm
    llvmPackages_18.lld
    libxml2
    zlib
  ];

  hardeningDisable = [ "all" ];
}
