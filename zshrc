#!/usr/bin/env zsh

# Git aliases
alias gs="git status"
alias gp="git pull"
alias gd="git diff"
alias gc="git commit"
alias gsm="git switch master || git switch main || git switch trunk"
alias gfo="git fetch origin"

launch() {
	$@ & ; disown ; exit
}

path+=("$HOME/src/zig/build/stage3/bin")
path+=("$HOME/.nix-profile/bin")
export PATH
